package ru.chelnokov.vehicle.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.chelnokov.vehicle.model.Car;
import ru.chelnokov.vehicle.model.dto.CarRqDto;
import ru.chelnokov.vehicle.model.dto.CarRsDto;
import ru.chelnokov.vehicle.model.dto.SearchDto;
import ru.chelnokov.vehicle.service.CarService;

@RestController
@RequestMapping("/cars")
@Tag(name = "car-controller", description = "Контроллер  для транспортных средств")
public class CarController {
    private final CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    @Operation(summary = "Выдача всех ТС")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<CarRsDto> getAllCars(@ParameterObject Pageable pageable) {
        return carService.getAll(pageable);
    }

    @Operation(summary = "Поиск ТС")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "/search")
    public Page<CarRsDto> searchCars(@ParameterObject Pageable pageable, @ParameterObject SearchDto searchDto) {
        return carService.search(pageable, searchDto);
    }

    @Operation(summary = "Одно ТС по id")
    @GetMapping(path = "/{id}")
    public CarRsDto getById(@PathVariable Long id) {
        return carService.getById(id);
    }

    @Operation(summary = "Добавление ТС")
    @PostMapping
    public ResponseEntity<CarRsDto> addCar(@Valid @RequestBody CarRqDto carRqDto) {
        var created = carService.saveCar(carRqDto, null);
        var url = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(created.getId())
                .toUri();
        return ResponseEntity.created(url).body(created);
    }

    @Operation(summary = "Добавить 20 случайных")
    @PostMapping(path = "/20random")
    public void add20Random() {
        carService.create20Random();
    }

    @Operation(summary = "Редактирование ТС")
    @PatchMapping(path = "/{id}")
    public CarRsDto editCar(@Valid @RequestBody CarRqDto carRqDto, @PathVariable Long id) {
        return carService.saveCar(carRqDto, id);
    }

    @Operation(summary = "Удаление ТС")
    @DeleteMapping(path = "/{id}")
    public CarRsDto delete(@PathVariable Long id) {
        return carService.delete(id);
    }



}
