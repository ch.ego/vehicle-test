package ru.chelnokov.vehicle.utils;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.chelnokov.vehicle.model.Car;

import java.util.List;
import java.util.function.Predicate;

@AllArgsConstructor
public class SearchPair {
    private String value;
    private Specification<Car> specification;

    public static final Predicate<String> NONE = (str) -> str.equals("----") ||str.equals("Все категории")||str.equals("Все типы");

    public static Specification<Car> getSpecs(List<SearchPair> searchPairList) {
        List<Specification<Car>> filled = searchPairList.stream()
                .filter(pair -> !NONE.test(pair.value))
                .map(pair -> pair.specification)
                .toList();
        return Specification.allOf(filled);
    }

}
