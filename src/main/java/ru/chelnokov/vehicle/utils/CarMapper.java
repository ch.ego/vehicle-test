package ru.chelnokov.vehicle.utils;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.chelnokov.vehicle.enums.Category;
import ru.chelnokov.vehicle.enums.Type;
import ru.chelnokov.vehicle.exception.BadRequestException;
import ru.chelnokov.vehicle.model.BrandModel;
import ru.chelnokov.vehicle.model.Car;
import ru.chelnokov.vehicle.model.dto.CarRqDto;
import ru.chelnokov.vehicle.model.dto.CarRsDto;

import java.time.Year;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Component
public class CarMapper {
    Pattern pattern = Pattern.compile("^(19|20|21)\\d{2}$");
    Matcher matcher;

    @Transactional
    public Car toCar(CarRqDto carRqDto) {
        Car given = Car.builder()
                .brandModel(BrandModel.builder().brand(carRqDto.getBrand()).model(carRqDto.getModel()).build())
                .type(Type.getByString(carRqDto.getType()).orElseThrow(() -> new BadRequestException("Неверный тип ТС")))
                .category(
                        Category.getByName(carRqDto.getCategory())
                                .orElseThrow(() -> new BadRequestException("Категория ТС указана некорректно"))
                                .name())
                .releaseYear(carRqDto.getReleaseYear())
                .trailer(Category.getByName(carRqDto.getCategory())
                        .orElseThrow(() -> new BadRequestException("Категория ТС указана некорректно"))
                        .trailer())
                .build();
        matcher = pattern.matcher(carRqDto.getReleaseYear());
        if (!matcher.find() || Integer.parseInt(carRqDto.getReleaseYear()) > Year.now().getValue()) {
            throw new BadRequestException("Год выпуска должен состоять из 4 цифр и быть в диапазоне 1950-текущий");
        }
        if (NumberUtils.isValid(NumberUtils.toCyrilic(carRqDto.getNumber().toUpperCase()))) {
            given.setNumber(NumberUtils.toCyrilic(carRqDto.getNumber().toUpperCase()));
        } else {
            throw new BadRequestException("Номер ТС не соответствует формату. - " + carRqDto.getNumber());
        }
        return given;
    }

    @Transactional
    public CarRsDto toResponse(Car car) {
        return CarRsDto.builder()
                .id(car.getId())
                .brand(car.getBrandModel().getBrand())
                .category(car.getCategory())
                .releaseYear(car.getReleaseYear())
                .number(car.getNumber())
                .type(car.getType().getPurpose())
                .model(car.getBrandModel().getModel())
                .trailer(car.getTrailer() ? "✓" : "✕")
                .build();
    }

    @Transactional
    public Page<CarRsDto> toResponse(Page<Car> cars) {
        return cars.map(this::toResponse);
    }
}
