package ru.chelnokov.vehicle.utils;

import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class NumberUtils {
    static final String NUMBER_REGEX = "([АВЕКМНОРСТУХ]\\d{3}(?<!000)[АВЕКМНОРСТУХ]{1,2})(\\d{2,3})|(\\d{4}(?<!0000)[АВЕКМНОРСТУХ]{2})(\\d{2})|(\\d{3}(?<!000)(C?D|[ТНМВКЕ])\\d{3}(?<!000))(\\d{2}(?<!00))|([ТСК][АВЕКМНОРСТУХ]{2}\\d{3}(?<!000))(\\d{2})|([АВЕКМНОРСТУХ]{2}\\d{3}(?<!000)[АВЕКМНОРСТУХ])(\\d{2})|([АВЕКМНОРСТУХ]\\d{4}(?<!0000))(\\d{2})|(\\d{3}(?<!000)[АВЕКМНОРСТУХ])(\\d{2})|(\\d{4}(?<!0000)[АВЕКМНОРСТУХ])(\\d{2})|([АВЕКМНОРСТУХ]{2}\\d{4}(?<!0000))(\\d{2})|([АВЕКМНОРСТУХ]{2}\\d{3}(?<!000))(\\d{2,3})|(^Т[АВЕКМНОРСТУХ]{2}\\d{3}(?<!000)\\d{2,3})";
    static List<String> abcCyr = List.of("А", "В", "Е", "К", "М", "Н", "О", "Р", "С", "Т", "У", "Х");
    static List<String> abcLat = List.of("A", "B", "E", "K", "M", "H", "O", "P", "C", "T", "Y", "X");
    static Random randomizer = new Random();

    public static boolean isValid(String number) {
        Pattern pattern = Pattern.compile(NUMBER_REGEX);
        Matcher matcher = pattern.matcher(number);
        boolean isValid = false;
        while (matcher.find()) {
            isValid = true;
        }
        return isValid;
    }

    public static String toCyrilic(String message) {
        StringBuilder builder = new StringBuilder();
        String[] msgArr = message.split("");
        String latStr = String.join("", abcLat);
        for (int i = 0; i < message.length(); i++) {
            if (!latStr.contains(msgArr[i])) {//цифра
                builder.append(msgArr[i]);
            }
            for (int x = 0; x < abcCyr.size(); x++) {
                if (Objects.equals(msgArr[i], abcLat.get(x))) {
                    builder.append(abcCyr.get(x));
                }
            }
        }
        return builder.toString();
    }

    public static String generateNumber() {
        StringBuilder sb = new StringBuilder();
        sb.append(abcCyr.get(randomizer.nextInt(abcCyr.size()))).append(randomizer.nextInt(11) + 1)
                .append(randomizer.nextInt(11) + 1).append(randomizer.nextInt(11) + 1)
                .append(abcCyr.get(randomizer.nextInt(abcCyr.size())))
                .append(abcCyr.get(randomizer.nextInt(abcCyr.size())))
                .append(randomizer.nextInt(11) + 1)
                .append(randomizer.nextInt(11) + 1);

        return sb.toString();
    }
}
