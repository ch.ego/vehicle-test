package ru.chelnokov.vehicle.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.chelnokov.vehicle.enums.Category;
import ru.chelnokov.vehicle.enums.Type;
import ru.chelnokov.vehicle.model.Car;
import ru.chelnokov.vehicle.model.dto.CarRqDto;
import ru.chelnokov.vehicle.repository.CarRepository;

import java.util.List;
import java.util.Random;

@Component
public class CarGenerator {
    private final CarRepository carRepository;
    private final CarMapper carMapper;
    List<String> mostPopularBrands = List.of("Lada", "Chevrolet", "BMW", "Ford");
    List<String> kindaModels = List.of("a1", "b4", "smth idk", "cx5");
    Random randomizer = new Random();

    @Autowired
    public CarGenerator(CarRepository carRepository, CarMapper carMapper) {
        this.carRepository = carRepository;
        this.carMapper = carMapper;
    }

    @Transactional
    public void save20Random() {
        for (int i = 0; i < 20; i++) {
            carRepository.save(getRandomCar());
        }
    }

    private Car getRandomCar() {
        return carMapper.toCar(CarRqDto.builder()
                .brand(mostPopularBrands.get(randomizer.nextInt(mostPopularBrands.size())))
                .model(kindaModels.get(randomizer.nextInt(kindaModels.size())))
                .type(Type.getRandom().getPurpose())
                .category(Category.getRandom().name())
                .number(NumberUtils.generateNumber())
                .releaseYear(String.valueOf(randomizer.nextInt(20) + 1980))
                .build());
    }
}
