package ru.chelnokov.vehicle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.chelnokov.vehicle.model.BrandModel;

@Repository
public interface BrandModelRepository extends JpaRepository<BrandModel, Long> {
    Boolean existsByBrandAndModel(String brand, String model);

    BrandModel getByBrandAndModel(String brand, String model);

}
