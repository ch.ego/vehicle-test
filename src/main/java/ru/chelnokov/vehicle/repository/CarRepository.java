package ru.chelnokov.vehicle.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.chelnokov.vehicle.model.BrandModel;
import ru.chelnokov.vehicle.model.Car;

import java.util.Optional;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    Page<Car> findAll(Specification<Car> or, Pageable pageable);

    Boolean existsByNumber(String number);

    Page<Car> findAllByNumber(String number, Pageable pageable);

    Page<Car> findAllByBrandModel(BrandModel brandModel, Pageable pageable);

    static Specification<Car> hasReleaseYear(String releaseYear) {
        return (carRoot, cq, cb) -> cb.like(carRoot.get("releaseYear"), "%" + releaseYear + "%");
    }

    static Specification<Car> hasType(String type) {
        return (carRoot, cq, cb) -> cb.like(cb.upper(carRoot.get("type")), "%" + type.toUpperCase() + "%");
    }

    static Specification<Car> hasCategory(String category) {
        return (carRoot, cq, cb) -> cb.like(carRoot.get("category"), "%" + category + "%");
    }

}
