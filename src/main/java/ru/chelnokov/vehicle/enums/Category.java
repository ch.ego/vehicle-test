package ru.chelnokov.vehicle.enums;


import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;

//категории ТС
public enum Category {
    A("A"),
    A1("A1"),
    B("B"),
    BE("BE"),
    B1("B1"),
    C("C"),
    CE("CE"),
    C1("C1"),
    C1E("C1E"),
    D("D"),
    DE("DE"),
    D1("D1"),
    D1E("D1E"),
    ALL("Все категории");

    private String description;

    Category(String description) {
        this.description = description;
    }

    public static Optional<Category> getByName(String name) {
        return Optional.of(valueOf(name));
    }

    public boolean trailer(){
        return this.name().contains("E");
    }

    public static List<String> descriptions(){
        return Arrays.stream(values()).map(v->v.description).toList();
    }

    public static Category getRandom() {
        return Arrays.stream(values()).filter(v -> v != ALL).toList().get(new Random().nextInt(values().length - 1));
    }
}
