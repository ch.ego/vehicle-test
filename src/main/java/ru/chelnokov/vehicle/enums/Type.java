package ru.chelnokov.vehicle.enums;

import java.util.*;


public enum Type {
    PASSENGER("Легковой автомобиль"),
    TRUCK("Грузовой автомобиль"),
    BUS("Автобус"),
    SPECIAL("Спецтехника"),
    ALL("Все типы");

    private final String purpose;

    Type(String purpose) {
        this.purpose = purpose;
    }

    public static List<String> purposes() {
        return Arrays.stream(values())
                .map(Type::getPurpose)
                .toList();
    }

    public String getPurpose() {
        return this.purpose;
    }

    public static Optional<Type> getByString(String purpose) {
        return Arrays.stream(values())
                .filter(v -> Objects.equals(v.purpose, purpose))
                .findFirst();
    }

    public static Type getRandom() {
        return Arrays.stream(values()).filter(v -> v != ALL).toList().get(new Random().nextInt(values().length - 1));
    }
}
