package ru.chelnokov.vehicle.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chelnokov.vehicle.enums.Type;
import ru.chelnokov.vehicle.exception.BadRequestException;
import ru.chelnokov.vehicle.exception.NotFoundException;
import ru.chelnokov.vehicle.model.BrandModel;
import ru.chelnokov.vehicle.model.Car;
import ru.chelnokov.vehicle.model.dto.CarRqDto;
import ru.chelnokov.vehicle.model.dto.CarRsDto;
import ru.chelnokov.vehicle.model.dto.SearchDto;
import ru.chelnokov.vehicle.repository.BrandModelRepository;
import ru.chelnokov.vehicle.repository.CarRepository;
import ru.chelnokov.vehicle.utils.CarGenerator;
import ru.chelnokov.vehicle.utils.CarMapper;
import ru.chelnokov.vehicle.utils.NumberUtils;
import ru.chelnokov.vehicle.utils.SearchPair;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

@Slf4j
@Service
public class CarService {
    private final CarRepository carRepository;
    private final CarMapper carMapper;
    private final BrandModelRepository brandModelRepository;
    private final CarGenerator carGenerator;

    @Autowired
    public CarService(CarRepository carRepository, CarMapper carMapper, BrandModelRepository brandModelRepository, CarGenerator carGenerator) {
        this.carRepository = carRepository;
        this.carMapper = carMapper;
        this.brandModelRepository = brandModelRepository;
        this.carGenerator = carGenerator;
    }

    @Transactional
    public Page<CarRsDto> getAll(Pageable pageable) {
        return carMapper.toResponse(carRepository.findAll(pageable));
    }

    @Transactional
    public Page<CarRsDto> search(Pageable pageable, SearchDto searchDto) {
        if (searchDto.getKeyword() == null) {
            return carMapper.toResponse(searchByCriteria(pageable, searchDto));
        } else {
            return searchWithKeyword(pageable, searchDto);
        }
    }

    @Transactional
    public Page<CarRsDto> searchWithKeyword(Pageable pageable, SearchDto searchDto) {
        String keyword = searchDto.getKeyword().trim().toUpperCase();
        String cyrilicUpper = NumberUtils.toCyrilic(keyword);

        if (NumberUtils.isValid(cyrilicUpper) && carRepository.existsByNumber(cyrilicUpper)) {
            return carMapper.toResponse(carRepository.findAllByNumber(cyrilicUpper, pageable));
        }

        List<BrandModel> bmList = brandModelRepository.findAll();
        bmList = bmList.stream()
                .filter(brandModel -> StringUtils.containsIgnoreCase(brandModel.getBrand(), keyword)
                        || StringUtils.containsIgnoreCase(keyword, brandModel.getBrand())
                        || StringUtils.containsIgnoreCase(brandModel.getModel(), keyword)
                        || StringUtils.containsIgnoreCase(keyword, brandModel.getModel())
                ).toList();

        List<Car> cars = new ArrayList<>();
        bmList.forEach(model -> {
            cars.addAll(carRepository.findAllByBrandModel(model, pageable).getContent());
        });
        List<Car> otherCars = searchByCriteria(pageable, searchDto).getContent();
        if (otherCars.isEmpty()) {
            if (!Objects.equals(searchDto.getYear(), "----")
                    || !Objects.equals(searchDto.getType(), "Все типы")
                    || !Objects.equals(searchDto.getCategory(), "Все категории")) {
                cars.clear();
            }
        } else {
            cars.retainAll(otherCars);
        }
        return new PageImpl<>(cars.stream().map(carMapper::toResponse).toList(), pageable, cars.size());

    }

    @Transactional
    public Page<Car> searchByCriteria(Pageable pageable, SearchDto searchDto) {
        Predicate<String> isNone = SearchPair.NONE;

        Specification<Car> hasCategory = CarRepository.hasCategory(searchDto.getCategory());
        Specification<Car> hasReleaseYear = CarRepository.hasReleaseYear(searchDto.getYear());
        Specification<Car> hasType = CarRepository.hasType(Type.getByString(searchDto.getType()).orElse(Type.ALL).toString());

        if (isNone.test(searchDto.getCategory()) && isNone.test(searchDto.getType()) && isNone.test(searchDto.getYear())) {
            return carRepository.findAll(pageable);
        } else {
            return carRepository.findAll(
                    SearchPair.getSpecs(
                            List.of(new SearchPair(searchDto.getCategory(), hasCategory),
                                    new SearchPair(searchDto.getYear(), hasReleaseYear),
                                    new SearchPair(searchDto.getType(), hasType))),
                    pageable);
        }
    }

    @Transactional
    public CarRsDto getById(Long id) {
        return carMapper.toResponse(carRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Тс не найдено")));
    }

    @Transactional
    public CarRsDto saveCar(CarRqDto carRqDto, Long id) {
        carRqDto.setNumber(NumberUtils.toCyrilic(carRqDto.getNumber().toUpperCase()));

        Car car = carMapper.toCar(carRqDto);
        if (brandModelRepository.existsByBrandAndModel(carRqDto.getBrand(), carRqDto.getModel())) {
            Long carId = brandModelRepository.getByBrandAndModel(carRqDto.getBrand(), carRqDto.getModel()).getId();
            car.setBrandModel(brandModelRepository.findById(carId)
                    .orElse(BrandModel.builder()
                            .brand(carRqDto.getBrand())
                            .model(carRqDto.getModel())
                            .build()));
        }
        if (carRepository.existsByNumber(carRqDto.getNumber())) {
            throw new BadRequestException("ТС с данным номером уже существует");
        }
        if (id != null) {
            if (carRepository.existsById(id) && !Objects.equals(getById(id).getNumber(), carRqDto.getNumber())) {
                if (carRepository.existsByNumber(carRqDto.getNumber()))
                    throw new BadRequestException("ТС с данным номером уже существует");
            }
            car.setId(id);
        }
        return carMapper.toResponse(carRepository.save(car));
    }

    @Transactional
    public CarRsDto delete(Long id) {
        Car deleted = carRepository.findById(id).orElseThrow(() -> new NotFoundException("ТС не найдено"));
        carRepository.delete(deleted);
        return carMapper.toResponse(deleted);
    }

    @Transactional
    public void create20Random() {
        carGenerator.save20Random();
    }
}
