package ru.chelnokov.vehicle.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.*;
import ru.chelnokov.vehicle.enums.Type;

@Entity(name = "cars")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cars")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "brandmodel_id")
    private BrandModel brandModel;

    @Column(nullable = false)
    @NotBlank(message = "Укажите категорию")
    private String category;

    @Column(unique = true, nullable = false)
    @NotBlank(message = "Укажите номер ТС")
    private String number;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Type type;//тип тс

    @Column(nullable = false)
    @NotBlank(message = "Укажите год выпуска")
    @Pattern(regexp = "^(19|20|21)\\d{2}$", message = "Год должен содержать 4 цифры(1900-2199)")
    private String releaseYear;//год выпуска

    @Column
    private Boolean trailer;//прицеп
}
