package ru.chelnokov.vehicle.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Entity(name = "brandmodel")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "brandmodel")
public class BrandModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    @NotBlank(message = "Укажите марку ТС")
    private String brand;
    @Column
    private String model;

    @Override
    public String toString() {
        return brand + " " + model;
    }
}
