package ru.chelnokov.vehicle.model.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class CarRsDto {
    private Long id;
    private String brand; //марка
    private String model;
    private String category;
    private String number;
    private String type;//тип тс
    private String releaseYear;//год выпуска
    private String trailer;//прицеп
}
