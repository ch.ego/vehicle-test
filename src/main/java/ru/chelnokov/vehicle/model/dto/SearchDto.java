package ru.chelnokov.vehicle.model.dto;


import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SearchDto {
    private String keyword;
    private String type;
    private String category;
    private String year;

    public SearchDto(String keyword, String type, String category, String year) {
        this.keyword = keyword;
        this.type = type == null ? "Все категории" : type;
        this.category = category == null ? "Все типы" : category;
        this.year = year == null ? "----" : year;
    }
}
