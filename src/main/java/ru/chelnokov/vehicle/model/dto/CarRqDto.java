package ru.chelnokov.vehicle.model.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Builder
public class CarRqDto {
    private String brand; //марка
    private String model;
    private String category;
    private String number;
    private String type;//тип тс
    private String releaseYear;
}
